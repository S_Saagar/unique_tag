package Parser;

import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;

import Model.BaseModel;
import Model.ResModel_ScaneData;

/**
 * Created by sc-96 on 13-Feb-18.
 */

public class Parser_ScaneData extends BaseParser {
    private static final String TAG = "Parser_ScaneData";

    @Override
    public BaseModel doParsing(JSONObject objJSON) {
        try {
            Gson gson = new Gson();
            ArrayList<BaseModel> resArr = new ArrayList<>();
            resArr.add(gson.fromJson(objJSON.toString(), ResModel_ScaneData.class));

            ResModel_ScaneData resModel_scaneData = new ResModel_ScaneData();
            resModel_scaneData.setModelArray(resArr);
            return resModel_scaneData;
        } catch (Exception e) {
            Log.e(TAG, "doParsing: Couldn't get data out of JSON");
            e.printStackTrace();
            return null;
        }
    }
}
