package Parser;

import org.json.JSONObject;

import Model.BaseModel;

/**
 * Created by sc-96 on 13-Feb-18.
 */

public abstract class BaseParser {
    public abstract BaseModel doParsing(JSONObject objJSON);
}
