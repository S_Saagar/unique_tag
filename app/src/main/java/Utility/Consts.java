package Utility;

/**
 * Created by sc-96 on 13-Feb-18.
 */

public class Consts {
    static final String PREF_NAME_SETTINGS = "Settings";
    static final String KEY_PROCESS = "Process";
    static final String KEY_ORDER_ID = "OrderID";
    static final String KEY_BASEURL = "BaseURL";
    public static final int GET_SCANNED_DATA = 100;

    //Process names
    public static final String MACHINEDEPARTMENT_5 = "MACHINEDEPARTMENT";
    public static final String ROLEDEPARTMENT_6 = "ROLEDEPARTMENT";
    public static final String CUTFOLDDEPARTMENT_7 = "CUTFDOLDDEPARTMENT";
    public static final String MUSONICDEPARTMENT_8 = "MUSONICDEPARTMENT";

    //Response_machinDetails
    public static final int SUCCESS = 1;
    public static final int FAILED = 0;

    public static String Data = "data";
    public static String Error = "Error";
}
