package Utility;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import static android.Manifest.permission_group.CAMERA;

/**
 * Created by sc-147 on 2/22/2018.
 */

public class PermissionManager {
    public static final int CAMERA_PERMMISION = 100;
    public static final int STORAGE_PERMISSION = 101;

    public static void CameraPermission(Activity activity) {
        ActivityCompat.requestPermissions(activity, new String[]{CAMERA}, CAMERA_PERMMISION);
    }

    public static boolean CheckCameraPermission(Context c) {
        return ContextCompat.checkSelfPermission(c, CAMERA) == PackageManager.PERMISSION_GRANTED;
    }

//    public static void StoragePermission(Activity activity) {
//        ActivityCompat.requestPermissions(activity, new String[]{STORAGE}, CAMERA_PERMMISION);
//    }
//
//    public static boolean CheckStoragePermission(Context c) {
//        return ContextCompat.checkSelfPermission(c, STORAGE) == PackageManager.PERMISSION_GRANTED;
//    }
}
