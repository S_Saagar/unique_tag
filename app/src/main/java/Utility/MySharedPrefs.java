package Utility;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by sc-96 on 13-Feb-18.
 */

public class MySharedPrefs {
    @SuppressLint("StaticFieldLeak")
    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor editor;


    public static void saveProcessName(Context c, String Process) {
        sharedPreferences = c.getSharedPreferences(Consts.PREF_NAME_SETTINGS, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString(Consts.KEY_PROCESS, Process);
        editor.apply();
    }

    public static String loadProcessName(Context c) {
        return c.getSharedPreferences(Consts.PREF_NAME_SETTINGS, Context.MODE_PRIVATE).getString(Consts.KEY_PROCESS, Consts.MACHINEDEPARTMENT_5);
    }

    public static void saveBaseURL(Context c, String URL) {
        sharedPreferences = c.getSharedPreferences(Consts.PREF_NAME_SETTINGS, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString(Consts.KEY_BASEURL, URL);
        editor.apply();
    }

    public static String loadBaseURL(Context c) {
        String defaultURL = "http://192.168.1.116/UniqueTagApi/api/Scanner/QRScanner";
        return c.getSharedPreferences(Consts.PREF_NAME_SETTINGS, Context.MODE_PRIVATE).getString(Consts.KEY_BASEURL, defaultURL);
    }

    public static void saveLastOrderID(Context c, String OrderID) {
        sharedPreferences = c.getSharedPreferences(Consts.PREF_NAME_SETTINGS, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString(Consts.KEY_ORDER_ID, OrderID);
        editor.apply();
    }

    public static String loadLastOrderID(Context c) {
        return c.getSharedPreferences(Consts.PREF_NAME_SETTINGS, Context.MODE_PRIVATE).getString(Consts.KEY_ORDER_ID, "");
    }
}