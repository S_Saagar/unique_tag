package com.example.sc_96.UniqueTag;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.jaredrummler.materialspinner.MaterialSpinner;

import org.json.JSONException;
import org.json.JSONObject;

import Model.BaseModel;
import Model.Response_machinDetails;
import Request.ApiCaller;
import Request.Get;
import Utility.Consts;
import Utility.MySharedPrefs;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.content.RestrictionsManager.RESULT_ERROR;

public class Setting_Activity extends BaseActivity implements MaterialSpinner.OnItemSelectedListener {

    @BindView(R.id.tv_selectProcess)
    TextView tvSelectProcess;
    @BindView(R.id.spinner)
    MaterialSpinner spinner;
    @BindView(R.id.edt_baseUrl_Ti)
    TextInputLayout edtBaseUrlTi;
    @BindView(R.id.btn_savedata)
    Button btnSavedata;

    String[] processStages = {Consts.MACHINEDEPARTMENT_5, Consts.ROLEDEPARTMENT_6, Consts.CUTFOLDDEPARTMENT_7, Consts.MUSONICDEPARTMENT_8};
    int getSelectedStage;
    String stage = "";
    @BindView(R.id.edt_baseUrl)
    EditText edtBaseUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        ButterKnife.bind(this);
        initSpinner();
        setUrledt();
    }

    private void setUrledt() {
        if (!MySharedPrefs.loadBaseURL(this).equals("")) {
            edtBaseUrl.setText(MySharedPrefs.loadBaseURL(this));
        } else {
            //Do nothing...
        }
    }

    private void initSpinner() {
        spinner = findViewById(R.id.spinner);
        spinner.setHint("Select Process");
        spinner.setItems(processStages);
        spinner.setSelectedIndex(0);
        spinner.setOnItemSelectedListener(this);
        getSelectedStage = spinner.getSelectedIndex();
        stage = processStages[getSelectedStage];
    }

    String ERROR_MSG_URL = "Please enter URL";

    @SuppressLint("ResourceAsColor")
    @OnClick(R.id.btn_savedata)
    public void onViewClicked() {
        if (getSelectedStage >= 0) {
            if (!TextUtils.isEmpty(edtBaseUrl.getText().toString())) {
                MySharedPrefs.saveProcessName(Setting_Activity.this, stage);
                MySharedPrefs.saveBaseURL(Setting_Activity.this, edtBaseUrl.getText().toString());
                Toast.makeText(this, "Data saved", Toast.LENGTH_SHORT).show();

                Get get = new Get();
                get.set(this, edtBaseUrl.getText().toString()+getString(R.string.url_getMachin)+ stage,
                        new Response_machinDetails(), new ApiCaller.OnResponse() {
                            @Override
                            public void ResponseData(BaseModel model) {

                                if (model instanceof Response_machinDetails)
                                {
                                    Response_machinDetails response_machinDetails = (Response_machinDetails) model;
                                    if (response_machinDetails.getSuccess()==1)
                                    {
                                        Intent intent = new Intent();
                                        intent.putExtra(Start_Activity.MACHINDETAILS_EXTRA,response_machinDetails);
                                        MySharedPrefs.saveProcessName(Setting_Activity.this, stage);
                                        MySharedPrefs.saveBaseURL(Setting_Activity.this, edtBaseUrl.getText().toString());
                                        setResult(RESULT_OK, intent);
                                        finish();
                                    }
                                }

                            }

                            @Override
                            public void OnError(VolleyError volleyError) {

                                String message;

                                if (volleyError instanceof NetworkError) {
                                    message = "Cannot connect to Internet...Please check your connection!";
                                    Log.v("NetworkError", message);

                                } else if (volleyError instanceof ServerError) {
                                    message = "The server could not be found. Please try again after some time!!";
                                    Log.v("ServerError", message);

                                } else if (volleyError instanceof AuthFailureError) {
                                    message = "Cannot connect to Internet...Please check your connection!";
                                    Log.v("AuthFailureError", message);

                                } else if (volleyError instanceof ParseError) {
                                    message = "Parsing error! Please try again after some time!!";
                                    Log.v("ParseError", message);

                                } else if (volleyError instanceof TimeoutError) {
                                    message = "Connection TimeOut! Please check your internet connection.";
                                    Log.v("TimeoutError", message);

                                } else {
                                    message = "Unknown Error";

                                }
                                finishActivityWithError(message);

                            }


                            @Override
                            public void OnDataError(JSONObject jsonObject) {


                                try {
                                    String error = jsonObject.getJSONObject(Consts.Data).getString(Consts.Error);
                                    finishActivityWithError(error);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        });
            } else {
                edtBaseUrl.requestFocus();
                edtBaseUrl.setError(ERROR_MSG_URL);
            }
        } else {
            spinner.requestFocus();
            spinner.setError("Please select process");
        }
    }

    private void finishActivityWithError(String error) {
        Intent intent = new Intent();
        intent.putExtra(Consts.Error,error);
//                                    intent.setData(Uri.parse(barcode.displayValue));
        setResult(RESULT_ERROR, intent);
        finish();
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onItemSelected(MaterialSpinner materialSpinner, int i, long l, Object o) {
        if (i >= 0) {
            stage = spinner.getText().toString();
            getSelectedStage = spinner.getSelectedIndex();
            spinner.setError(null);


        } else {
            //Do nothing..
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}