package com.example.sc_96.UniqueTag;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.SparseArray;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.vision.barcode.Barcode;

import java.util.List;

import Utility.MySharedPrefs;
import butterknife.BindView;
import butterknife.ButterKnife;
import info.androidhive.barcode.BarcodeReader;

public class Scanner_Activity extends BaseActivity implements BarcodeReader.BarcodeReaderListener {

    BarcodeReader barcodeReader;
    @BindView(R.id.tv_ProcessName)
    TextView tvProcessName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scanner);
        ButterKnife.bind(this);
        initProcessName();
    }

    private void initQRCodeReader() {
        barcodeReader = (BarcodeReader) getSupportFragmentManager().findFragmentById(R.id.barcode_scanner);
    }

    private void initProcessName() {
        String processName = MySharedPrefs.loadProcessName(this);
        if (!processName.equals("")) {
            initQRCodeReader();
            processName = "Process Name : " + MySharedPrefs.loadProcessName(this);
            tvProcessName.setText(processName);
        } else {
            Toast.makeText(this, "Please select process", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this, Setting_Activity.class);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void onScanned(Barcode barcode) {
        barcodeReader.playBeep();
        Intent intent = new Intent();
        MySharedPrefs.saveLastOrderID(this, barcode.displayValue);
        intent.setData(Uri.parse(barcode.displayValue));
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onScannedMultiple(List<Barcode> barcodes) {

    }

    @Override
    public void onBitmapScanned(SparseArray<Barcode> sparseArray) {

    }

    @Override
    public void onScanError(String errorMessage) {

    }

    @Override
    public void onCameraPermissionDenied() {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}