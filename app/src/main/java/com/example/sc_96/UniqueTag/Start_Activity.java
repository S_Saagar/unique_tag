package com.example.sc_96.UniqueTag;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import Interface.onReply;
import Model.ReqModel_ScaneData;
import Model.ResModel_ScaneData;
import Model.Response_machinDetails;
import Parser.BaseParser;
import Parser.Parser_ScaneData;
import Request.Request_UploadQRScaneData;
import Utility.ConnectionManager;
import Utility.Consts;
import Utility.MySharedPrefs;
import adpter.Rv_mainAdpter;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static Utility.Consts.GET_SCANNED_DATA;
import static android.content.RestrictionsManager.RESULT_ERROR;

public class Start_Activity extends BaseActivity implements onReply {

    public static final String MACHINDETAILS_EXTRA = "MACHIN_DETAILS";
    private static final int GET_MACHIN_DETAILS = 200;
    @BindView(R.id.btn_start_scanning)
    Button btnStartScanning;
    @BindView(R.id.btn_settings)
    Button btnSettings;
    @BindView(R.id.tv_OrderID_title)
    TextView tvOrderIDTitle;
    @BindView(R.id.tv_OrderID)
    TextView tvOrderID;
    @BindView(R.id.linearLayout)
    LinearLayout linearLayout;
    ProgressDialog p;
    Intent intent;
    @BindView(R.id.RV_nums)
    RecyclerView rv_Nums;

    public String mMachinNum;
    @BindView(R.id.TV_select_machin)
    TextView TVSelectMachin;
    private Rv_mainAdpter adpter;
    private Response_machinDetails response_machinDetails;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        ButterKnife.bind(this);
        setOrderTv();

    }

    private void setRecyclerView(Response_machinDetails response_machinDetails) {

        rv_Nums.setLayoutManager(new GridLayoutManager(this, 10));
        adpter = new Rv_mainAdpter(this, response_machinDetails);

        rv_Nums.setAdapter(adpter);


    }

    private void setOrderTv() {
        if (!MySharedPrefs.loadLastOrderID(this).equals("")) {
            tvOrderID.setText(MySharedPrefs.loadLastOrderID(this));
        } else {
            //Do nothing...
        }
    }

    @OnClick({R.id.btn_start_scanning, R.id.btn_settings})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_start_scanning:
                if (ConnectionManager.Check(this)) {

                    if (mMachinNum != null) {
                        intent = new Intent(this, Scanner_Activity.class);
                        startActivityForResult(intent, GET_SCANNED_DATA);
                    } else {
                        AlertDialog(getString(R.string.Alert_select_machin), Start_Activity.this);
                    }
                } else {
                    String INTERNET_CONNECTION_ERROR = "No internet connection";
                    AlertDialog(INTERNET_CONNECTION_ERROR, this);
                }
                break;
            case R.id.btn_settings:
                intent = new Intent(this, Setting_Activity.class);
                startActivityForResult(intent, GET_MACHIN_DETAILS);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case GET_SCANNED_DATA:
                switch (resultCode) {
                    case RESULT_OK:
                        p = new ProgressDialog(this);
                        p.setTitle("Uploading data");
                        p.setMessage("Please wait...");
                        p.setCancelable(false);
                        p.show();
                        Uri intentData = data.getData();
                        assert intentData != null;
                        tvOrderID.setText(intentData.toString());
                        sendQRcodeDataToserver(intentData.toString());
                        break;
                }
                break;

            case GET_MACHIN_DETAILS:

                if (resultCode == RESULT_OK) {
                    TVSelectMachin.setVisibility(View.VISIBLE);
                    response_machinDetails = (Response_machinDetails) data.getSerializableExtra(MACHINDETAILS_EXTRA);
                    if (response_machinDetails != null) {

                        setRecyclerView(response_machinDetails);
                    }

                } else if (resultCode == RESULT_ERROR) {
                    TVSelectMachin.setVisibility(View.GONE);
                    if (response_machinDetails != null) {
                        response_machinDetails.getData().clear();
                        setRecyclerView(response_machinDetails);
                    }
                    AlertDialog(data.getStringExtra(Consts.Error), Start_Activity.this);
                }

                break;
        }
    }

    private void sendQRcodeDataToserver(String s) {
        ReqModel_ScaneData reqModel_scaneData = new ReqModel_ScaneData();
        reqModel_scaneData.setOrderNo(s);

        reqModel_scaneData.setStatus(MySharedPrefs.loadProcessName(this));

        if (mMachinNum != null) {
            reqModel_scaneData.setMachinNum(mMachinNum);
        } else {
            AlertDialog(getString(R.string.Alert_select_machin), this);
        }
        Request_UploadQRScaneData request_uploadQRScaneData = new Request_UploadQRScaneData(this);
        request_uploadQRScaneData.sendRequest(this, reqModel_scaneData);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onPopulate(JSONObject objJson, BaseParser baseParser) {
        if (baseParser instanceof Parser_ScaneData) {
            if (objJson != null) {
                p.dismiss();
                ResModel_ScaneData supermodel = (ResModel_ScaneData) baseParser.doParsing(objJson);
                ResModel_ScaneData resModel_scaneData = (ResModel_ScaneData) supermodel.getModelArray().get(0);
                if (resModel_scaneData.getSuccess() == Consts.SUCCESS) {
                    Toast.makeText(this, "Data uploaded successfully.", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(this, resModel_scaneData.getData().getError(), Toast.LENGTH_LONG).show();
                }
            } else {
                p.dismiss();
                Toast.makeText(this, "Something went wrong check internet or URL.", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void NetworkError(String msg) {
        p.dismiss();
        ShowToast(msg, true, this);
    }

    @Override
    public void ServerError(String msg) {
        p.dismiss();
        ShowToast(msg, true, this);
    }

    @Override
    public void AuthFailureError(String msg) {
        p.dismiss();
        ShowToast(msg, true, this);
    }

    @Override
    public void ParseError(String msg) {
        p.dismiss();
        ShowToast(msg, true, this);
    }

    @Override
    public void TimeoutError(String msg) {
        p.dismiss();
        ShowToast(msg, true, this);
    }

    @Override
    public void UnknowError(String msg) {
        p.dismiss();
        ShowToast(msg, true, this);
    }
}