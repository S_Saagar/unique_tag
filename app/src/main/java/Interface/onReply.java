package Interface;

import org.json.JSONObject;

import Parser.BaseParser;

/**
 * Created by sc-147 on 2/16/2018.
 */

public interface onReply {
    void onPopulate(JSONObject objJson, BaseParser baseParser);

    void NetworkError(String msg);

    void ServerError(String msg);

    void AuthFailureError(String msg);

    void ParseError(String msg);

    void TimeoutError(String msg);

    void UnknowError(String msg);
}
