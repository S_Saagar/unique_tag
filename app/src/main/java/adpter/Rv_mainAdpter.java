package adpter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sc_96.UniqueTag.R;
import com.example.sc_96.UniqueTag.Start_Activity;

import Model.Response_machinDetails;

/**
 * Created by samarth on 6/18/2018.
 */

public class Rv_mainAdpter extends RecyclerView.Adapter<Rv_mainAdpter.CustomeViewHolder> {

    private Context mContext;
    TextView previousTV = null;
    Response_machinDetails mResponse_machinDetails;

    public Rv_mainAdpter(Context context, Response_machinDetails response_machinDetails) {
        mContext = context;
        mResponse_machinDetails = response_machinDetails;
    }

    @Override
    public CustomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(mContext);


        return new CustomeViewHolder(layoutInflater.inflate(R.layout.rv_main_list,null));
    }

    @Override
    public void onBindViewHolder(final CustomeViewHolder holder, final int position) {

        if (mResponse_machinDetails!=null) {
            holder.tv_num.setText(String.valueOf(mResponse_machinDetails.getData().get(position).getId()));

            holder.tv_num.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {

//                Toast.makeText(mContext, String.valueOf(position+1), Toast.LENGTH_SHORT).show();
                    if (previousTV != null) {

                        previousTV.setBackground(mContext.getDrawable(R.drawable.selected_border));
                        previousTV.setTextColor(mContext.getResources().getColor(R.color.colorBlack));
                    }
                    previousTV = holder.tv_num;
                    holder.tv_num.setBackground(mContext.getDrawable(R.drawable.selected_border2));
                    holder.tv_num.setTextColor(mContext.getResources().getColor(R.color.colorWhite));
//                    holder.tv_num.setBackground(mContext.getResources().getDrawable(R.drawable.selected_border));
                    if (mContext instanceof Start_Activity)
                    {
                        ((Start_Activity) mContext).mMachinNum = String.valueOf(mResponse_machinDetails.getData().get(position).getId());
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mResponse_machinDetails.getData().size();
    }

    class CustomeViewHolder extends RecyclerView.ViewHolder
    {

        TextView tv_num;

        public CustomeViewHolder(View itemView) {
            super(itemView);

            tv_num = itemView.findViewById(R.id.rv_tv);
        }
    }
}
