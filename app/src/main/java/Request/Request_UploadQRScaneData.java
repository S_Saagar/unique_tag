package Request;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;

import Interface.onReply;
import Model.BaseModel;
import Model.ReqModel_ScaneData;
import Parser.Parser_ScaneData;
import Utility.MySharedPrefs;

/**
 * Created by sc-96 on 13-Feb-18.
 */

public class Request_UploadQRScaneData extends BaseRequest {


    @SuppressLint("StaticFieldLeak")
    private static Context context;

    public Request_UploadQRScaneData(Context c) {
        context = c;
    }

    @Override
    public void sendRequest(onReply objReplyMethod, BaseModel model) {
        onReplyDelegate = objReplyMethod;
        objParser = new Parser_ScaneData();
        c = context;

        this.URL = MySharedPrefs.loadBaseURL(context);

        Gson gson = new Gson();

        String json = gson.toJson(model, ReqModel_ScaneData.class);

        Log.v("SendRequest: Json->>", "" + json);

        sendRequestToServer(json);
    }
}
