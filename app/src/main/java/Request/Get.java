package Request;

import android.app.ProgressDialog;
import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONException;
import org.json.JSONObject;

import Model.BaseModel;

/**
 * Created by samarth on 6/17/2018.
 */

public class Get extends ApiCaller {

    public void set(Context context, String URL, BaseModel modelClassObject, OnResponse onResponse) {

        super.context = context;
        super.requestQueue  = Volley.newRequestQueue(context);
        super.Url = URL;
        super.modelClassObject = modelClassObject;
        super.onResponse = onResponse;
        call();
    }

    private void call() {

        if (isProgressShow)
        {
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage(progressText);
            progressDialog.show();
        }

        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, Url,

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if (isProgressShow)
                        {
                            stopProgressDialogue();
                        }

                        Gson gson = new Gson();

//                            Log.e(Tag,response.toString());

                        try {

                            modelClassObject = gson.fromJson(response.toString(), modelClassObject.getClass());
                            onResponse.ResponseData(modelClassObject);
                        }
                        catch (JsonSyntaxException e)
                        {
                            e.printStackTrace();
                            try {
                                onResponse.OnDataError(new JSONObject(response));
                            } catch (JSONException e1) {
                                e1.printStackTrace();
                            }
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (isProgressShow)
                        {
                            stopProgressDialogue();
                        }

                        onResponse.OnError(error);
                    }
                }
        );
        requestQueue.add(jsonObjectRequest);

    }

}
