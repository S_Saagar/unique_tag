package Request;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import Interface.onReply;
import Model.BaseModel;
import Parser.BaseParser;

/**
 * Created by sc-96 on 13-Feb-18.
 */

public abstract class BaseRequest {

    private static final String OUTPUT = "output";
    private static final String NETWORK_ERROR = "Network is unreachable";
    private JSONObject jsonObject;
    String URL;
    BaseParser objParser;
    onReply onReplyDelegate;
    private JSONObject jsonRequest;
    Context c;
    private static JsonObjectRequest jsObjRequest;
    private String message;

    public abstract void sendRequest(onReply objReplyMethod, BaseModel model);

    void sendRequestToServer(String jsonString) {
        Log.e("Json ", jsonString);
        volleyReq(jsonString);
    }

    private void volleyReq(String jsonStrings) {
        try {
            jsonRequest = new JSONObject(String.valueOf(jsonStrings));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, URL, jsonRequest, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            jsonObject = new JSONObject();
                            jsonObject.put(OUTPUT, new JSONObject(response.toString()));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if (jsonObject != null) {
                            try {
                                onReplyDelegate.onPopulate(jsonObject.getJSONObject(OUTPUT), objParser);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            onReplyDelegate.onPopulate(null, objParser);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        // TODO Auto-generated method stub
                        if (volleyError instanceof NetworkError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                            Log.v("NetworkError", message);
                            onReplyDelegate.NetworkError(message);
                        } else if (volleyError instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                            Log.v("ServerError", message);
                            onReplyDelegate.ServerError(message);
                        } else if (volleyError instanceof AuthFailureError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                            Log.v("AuthFailureError", message);
                            onReplyDelegate.AuthFailureError(message);
                        } else if (volleyError instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                            Log.v("ParseError", message);
                            onReplyDelegate.ParseError(message);
                        } else if (volleyError instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                            Log.v("TimeoutError", message);
                            onReplyDelegate.TimeoutError(message);
                        } else {
                            message = "Unknown Error";
                            onReplyDelegate.UnknowError(message);
                        }
                    }
                });
        MySingleton.getInstance(c).addToRequestQueue(jsObjRequest);
    }

    static int getRetryCount() {
        return jsObjRequest.getRetryPolicy().getCurrentRetryCount();
    }
}
