package Request;


import android.app.ProgressDialog;
import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import Model.BaseModel;

/**
 * Created by samarth on 5/30/2018.
 */

public class ApiCaller  {

    protected Context context;
    protected String Url;
    protected JSONObject jsonObject = new JSONObject();
    protected RequestQueue requestQueue;
    protected OnResponse onResponse ;
    protected Boolean isProgressShow = true;
    protected String progressText = "Loading";
    protected BaseModel modelClassObject;
    protected ProgressDialog progressDialog = null;
//    public Post post ;
//    public Get get;
    protected String Tag = "ApiCallerResponce: ";

//    public ApiCaller() {
//        post = new Post();
//        get = new Get();
//    }

    public ApiCaller showprogressDilogue(boolean b) {

        this.isProgressShow = b;

        return this;
    }

    public ApiCaller setProgressText(String progressText) {

        this.progressText = progressText;

        return this;
    }

    protected void stopProgressDialogue() {

            progressDialog.dismiss();

    }

    public interface OnResponse {

        void ResponseData(BaseModel model);
        void OnError(VolleyError error);
        void OnDataError(JSONObject jsonObject);
    }

//    public class Post extends ApiCaller {
//
//        public void set(Context context, String URL, JSONObject jsonObject, BaseModel modelClassObject, OnResponse onResponse) {
//
//            super.context = context;
//            super.requestQueue  = Volley.newRequestQueue(context);
//            super.jsonObject = jsonObject;
//            super.Url = URL;
//            super.modelClassObject = modelClassObject;
//            super.onResponse = onResponse;
//            call();
//        }
//
//        public void call() {
//
//            if (isProgressShow)
//            {
//                progressDialog = new ProgressDialog(context);
//                progressDialog.setMessage(progressText);
//                progressDialog.show();
//            }
//
//            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, Url, jsonObject,
//
//                    new Response_machinDetails.Listener<JSONObject>() {
//                        @Override
//                        public void onResponse(JSONObject response) {
//
//                            if (isProgressShow)
//                            {
//                                stopProgressDialogue();
//                            }
//
//                            Gson gson = new Gson();
//
////                            Log.e(Tag,response.toString());
//
//                            modelClassObject = gson.fromJson(response.toString(),modelClassObject.getClass());
//
//                            onResponse.ResponseData(modelClassObject);
//                        }
//                    },
//                    new Response_machinDetails.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//
//                            onResponse.OnError(error);
//                        }
//                    }
//            );
//            requestQueue.add(jsonObjectRequest);
//
//        }
//
//    }

//    public class Get extends ApiCaller {
//
//        public void set(Context context, String URL, BaseModel modelClassObject, OnResponse onResponse) {
//
//            super.context = context;
//            super.requestQueue  = Volley.newRequestQueue(context);
//            super.Url = URL;
//            super.modelClassObject = modelClassObject;
//            super.onResponse = onResponse;
//            call();
//        }
//
//        public void call() {
//
//            if (isProgressShow)
//            {
//                progressDialog = new ProgressDialog(context);
//                progressDialog.setMessage(progressText);
//                progressDialog.show();
//            }
//
//            StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, Url,
//
//                    new Response_machinDetails.Listener<String>() {
//                        @Override
//                        public void onResponse(String response) {
//
//                            if (isProgressShow)
//                            {
//                                stopProgressDialogue();
//                            }
//
//                            Gson gson = new Gson();
//
////                            Log.e(Tag,response.toString());
//
//                            modelClassObject = gson.fromJson(response,modelClassObject.getClass());
//
//                            onResponse.ResponseData(modelClassObject);
//                        }
//                    },
//
//                    new Response_machinDetails.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//
//                        }
//                    });
//
//            requestQueue.add(jsonObjectRequest);
//
//        }
//
//    }

}
