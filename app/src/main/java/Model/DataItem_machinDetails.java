package Model;

import java.io.Serializable;

public class DataItem_machinDetails implements Serializable{
	private String machineNo;
	private Object entrydt;
	private int Id;
	private String type;

	public void setMachineNo(String machineNo){
		this.machineNo = machineNo;
	}

	public String getMachineNo(){
		return machineNo;
	}

	public void setEntrydt(Object entrydt){
		this.entrydt = entrydt;
	}

	public Object getEntrydt(){
		return entrydt;
	}

	public void setId(int id){
		this.Id = id;
	}

	public int getId(){
		return Id;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}

	@Override
 	public String toString(){
		return 
			"DataItem_machinDetails{" +
			"machineNo = '" + machineNo + '\'' + 
			",entrydt = '" + entrydt + '\'' + 
			",Id = '" + Id + '\'' +
			",type = '" + type + '\'' + 
			"}";
		}
}
