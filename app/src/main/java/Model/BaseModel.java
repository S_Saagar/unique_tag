package Model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by sc-96 on 13-Feb-18.
 */

public class BaseModel implements Serializable {

    protected ArrayList<BaseModel> modelArray;

    public ArrayList<BaseModel> getModelArray() {
        return modelArray;
    }

    public void setModelArray(ArrayList<BaseModel> modelArray) {
        this.modelArray = modelArray;
    }
}
