package Model;

import java.io.Serializable;
import java.util.List;

public class Response_machinDetails extends BaseModel implements Serializable{
	private List<DataItem_machinDetails> data;
	private int success;

	public void setData(List<DataItem_machinDetails> data){
		this.data = data;
	}

	public List<DataItem_machinDetails> getData(){
		return data;
	}

	public void setSuccess(int success){
		this.success = success;
	}

	public int getSuccess(){
		return success;
	}

	@Override
 	public String toString(){
		return 
			"Response_machinDetails{" +
			"data = '" + data + '\'' + 
			",success = '" + success + '\'' + 
			"}";
		}
}