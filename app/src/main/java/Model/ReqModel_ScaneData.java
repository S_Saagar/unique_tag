package Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sc-96 on 13-Feb-18.
 */

public class ReqModel_ScaneData extends BaseModel {
    @SerializedName("orderNo")
    @Expose
    private String orderNo;
    @SerializedName("status")
    @Expose
    private String status;

    public String getMachinNum() {
        return machinNum;
    }

    public void setMachinNum(String machinNum) {
        this.machinNum = machinNum;
    }

    @SerializedName("machineNo ")
    @Expose
    private String machinNum;


    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}